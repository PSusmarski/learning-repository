class UsersController < ApplicationController

  # GET /users
  # GET /users.json
  def index
    @users = User.order(:email)
  end
end
